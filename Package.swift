// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let version = "1.0.0"

let package = Package(
    name: "CloudPaymentsKit",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "CloudPaymentsKit",
            targets: ["CloudPaymentsKit"]),
    ],
    dependencies: [
        .package(url: "https://dhomes@bitbucket.org/dhomes/spm_cpcorekit.git", branch: "master"),
		.package(url: "https://dhomes@bitbucket.org/dhomes/spm_cpfundskit.git", branch: "master")
    ]
)
